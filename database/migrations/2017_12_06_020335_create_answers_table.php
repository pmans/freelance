<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('explicit_id')->unsigned();
            $table->integer('pakar_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('explicit_id')->references('id')->on('explicits')->onDelete('CASCADE');
            $table->foreign('pakar_id')->references('id')->on('pakars')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
