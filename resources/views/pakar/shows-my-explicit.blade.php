@extends('layoutsPakar.master')
@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>
        
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-book"></span>YOUR DATA KNOWLEDGES <a href="{{ route('add-explicit') }}"><button type="submit" class="btn btn-info btn-fill btn-wd"><span class="glyphicon glyphicon-plus"></span>Add Explicit</button></a>
                </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="table">
                            <table width="100%" class="table table-hover" id="table">
                                <thead>
                                    <tr>
                                        <th><h4><center>#</center></h4></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($explicits as $explicit)
                                    @if ( $explicit->user_id == Auth::user()->id)  
                                    <?php 
                                    $view = $explicit->description ;
                                    $content = substr($view ,0 ,200); 
                                    ?>
                                    <tr>
                                        <td><h4>{{ $loop->index+1 }}</h4></td>
                                        <td>
                                        <div class="post-detail">
                                            <h4><a href="{{ route('detail-explicit', $explicit->id) }}"><b>{{ $explicit->title }}</b></a></h4>
                                            <ul class="list-inline">
                                                <li>
                                                    <i><span class="label label-info"><span class="glyphicon glyphicon-calendar"></span> {{ $explicit->created_at }}</span>

                                                    <span class="label label-info"><span class="glyphicon glyphicon-time"></span> {{ $explicit->created_at->diffForHumans() }}</span>

                                                    <span class="label label-info"> <span class="glyphicon glyphicon-user"></span> Posting by {{ $explicit->user->name }}</i></span>
                                                </li>
                                            </ul>
                                        </div>


                                        

                                        <?php echo $content; ?>...<br>
                                        <a href="{{ route('detail-explicit', $explicit->id) }}"><button type="submit" class="btn btn-info btn-fill btn-xs"> Read More...</button></a>
                                        <a href="{{ $explicit->id }}/delete-explicit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</a>
                                        <a href="{{ route('edit.explicit.pakar', $explicit->id) }}"><button type="submit" class="btn btn-primary btn-fill btn-xs"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach

                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection