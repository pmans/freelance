@extends('layoutsAdmin.master')
@section('content')
<style type="text/css">
    .table td {
     text-align: center;   
 }
 .table th {
     text-align: center;   
 }
</style>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span>TACITS DATA
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="table">
                            <table width="100%" class="table table-hover" id="table">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Date & Time</th>
                                    <th>Complain</th>
                                    <th>Action</th>
                                    <th>Information</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($tacits as $tacit)
                                @if ( $tacit->user_id == Auth::user()->id)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $tacit->created_at }}</td>
                                    <td>{{ $tacit->complain }}</td>
                                    <td>{{ $tacit->action }}</td>
                                    <td>{{ $tacit->information }}</td>
                                    <td>
                                        <?php
                                        if ($tacit->status == 'Pending') {
                                            echo"<button type='button' id='upgrade' class='btn btn-xs btn-danger'>";
                                            echo$tacit->status;
                                            echo "</button>";}else{
                                                echo"<button type='button' id='upgrade' class='btn btn-xs btn-info'>";
                                               echo $tacit->status;
                                                echo "</button>";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="{{ route('edit.tacit', $tacit->id) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span>Edit</a> <a href="{{ $tacit->id }}/delete-tacit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</a> 
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection