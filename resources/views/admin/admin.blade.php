@extends('layoutsAdmin.master')
@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div><!--/.row-->

  
       
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>LIST ADMIN <a href="{{ route('show.add.admin') }}"><button type="submit" class="btn btn-info btn-fill btn-wd"><span class="glyphicon glyphicon-plus"></span>Add Admin</button></a></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="table">
                            <table width="100%" class="table table-hover" id="table2">
                                <thead >
                                  <tr>
                                    <th style="text-align:center">#</th>
                                    <th style="text-align:center">Name</th>
                                    <th style="text-align:center">Email</th>
                                    <th style="text-align:center">Action</th>
                                </thead>

                                <tbody>
                                @foreach ($admins as $admin)
                                
                                  <tr style="text-align:center">
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $admin->name }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td>
                                    <a href="{{ route('edit.admin', $admin->id) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="{{ $admin->id }}/delete-admin" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</a> 
                                    </td>
                                   </tr>
                                
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
      
@endsection