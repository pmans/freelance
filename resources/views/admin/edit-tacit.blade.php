@extends('layoutsAdmin.master')
@section('content')
   
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><span class="glyphicon glyphicon-list-edit"></span>EDIT TACIT</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form class="form-horizontal" method="POST" action="{{ route('edit.tacit', $tacit) }}">
                                {{ csrf_field() }}
                                {{ method_field('patch') }}      

                                <div class="form-group">
                                    <label>COMPLAIN</label>
                                    <textarea name="complain" class="form-control" id="exampleFormControlTextarea1" rows="6">{{ $tacit->complain }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>ACTION</label>
                                    <textarea name="action" class="form-control" id="exampleFormControlTextarea1" rows="6">{{ $tacit->action }}</textarea>
                                    
                                </div>

                                <div class="form-group">
                                    <label>INFORMATION</label>
                                    <input name="information" class="form-control" placeholder="Information" value="{{ $tacit->information }}">
                                </div>

                                <div class="form-group">
                                    <label>STATUS</label>
                                    <select name="status" class="form-control form-control-sm">
                                    <option value="Pending">PENDING</option>
                                    <option value="End">END</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-info btn-fill btn-wd"><span class="glyphicon glyphicon-edit"></span> Edit Tacit </button>
                
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
