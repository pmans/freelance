@extends('layoutsAdmin.master')
@section('content')
   
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><span class="glyphicon glyphicon-edit"></span>EDIT EXPLICIT</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form class="form-horizontal" method="POST" action="{{ route('edit.explicit', $explicit) }}">
                                {{ csrf_field() }}
                                {{ method_field('patch') }}      

                                <div class="form-group">
                                    <label>TITLE</label>
                                    <input name="title" class="form-control" placeholder="Title" value="{{ $explicit->title }}">
                                </div>

                                <div class="form-group">
                                    <label>DESCRIPTION</label>
                                    <textarea name="description" id="ckeditor" required>{{ $explicit->description }}</textarea>
                                </div>
                
                                <div class="form-group">
                                    <label>URL</label>
                                    <input name="url" class="form-control" value="{{ $explicit->url }}">
                                </div>
                        
                                <button type="submit" class="btn btn-info btn-fill btn-wd"><span class="glyphicon glyphicon-edit"></span> Edit Explicit</button>
                
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection