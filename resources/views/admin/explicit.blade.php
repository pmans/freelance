@extends('layoutsAdmin.master')
@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>
        
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-book"></span>MANAGE EXPLICITS <a href="{{ route('show.add.explicit') }}"><button type="submit" class="btn btn-info btn-fill btn-wd"><span class="glyphicon glyphicon-plus"></span>Add Explicit</button></a>
                </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="table">
                            <table width="100%" class="table table-hover" id="table2">
                                <thead >
                                  <tr>
                                    <th style="text-align:center">#</th>
                                    <th style="text-align:center">Title</th>
                                    <th style="text-align:center">Time</th>
                                    <th style="text-align:center">By</th>
                                    <th style="text-align:center">Explicit</th>
                                    <th style="text-align:center">Action</th>
                                </thead>

                                <tbody>
                                @foreach ($explicits as $explicit)
                                 
                                    <?php 
                                    $view = $explicit->description ;
                                    $content = substr($view ,0 ,25); 
                                    ?>
                                
                                  <tr style="text-align:center">
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $explicit->title }}</td>
                                    <td>{{ $explicit->created_at }}</td>
                                    <td>{{ $explicit->user->name }}</td>
                                    <td> <?php echo $content; ?>...</td>
                                    <td>
                                    <a href="{{ route('edit.explicit', $explicit->id) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span>Edit</a> <a href="{{ $explicit->id }}/delete-explicit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</a> 
                                    </td>

                                   </tr>
                                
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection