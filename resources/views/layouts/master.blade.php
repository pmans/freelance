<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>KMS System</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-table.css" rel="stylesheet">
    <script src="/assets/js/ckeditorbasic/ckeditor.js"></script> 
    <link href="/assets/css/dataTables.bootstrap.css" rel="stylesheet">
    <style type="text/css">

    </style>
</head>
<body>  
        @include('layouts.navbar')
        @include('layouts.sidebar')
        @include('layouts.alert.alerts')
        @yield('content')
</body>
    <script>CKEDITOR.replace('ckeditor');</script>
    <script src="/assets/js/jquery-1.11.1.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.dataTables.js"></script>
    <script src="/assets/js/dataTables.bootstrap.js"></script>

<!-- DATATABLE -->
<script>
            $(document).ready(function () {
                $('#table').dataTable();
            });
</script>

<script>
            $(document).ready(function () {
                $('#table2').dataTable();
            });
</script>