<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">

@if (session('info'))
    <div class="alert alert-info">
        <b><span class="glyphicon glyphicon-ok"></span> {{ session('info') }}</b>
    </div>
@endif

@if (session('danger'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span><b> {{ session('danger') }}</b>
    </div>
@endif
        </div>
    </div>
</div>