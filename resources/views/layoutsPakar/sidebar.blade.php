
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

        <ul class="nav menu">
            <li class="list-group-item">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" ></span> {{ Auth::user()->name }} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <span class="glyphicon glyphicon-log-out"></span> Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}
                    </form>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="nav menu">
            <li><a href="{{ route('pakar.home') }}"><span class="glyphicon glyphicon-dashboard"></span> DASBOARD </a></li>
            <li class="parent ">  
                <a href="#">
                    <span data-toggle="collapse" href="#sub-item-1"><span class="glyphicon glyphicon-th"></span> EXPLICIT <span class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span></span>
                </a>
                <ul class="children collapse" id="sub-item-1">
                    <li>
                        <a href="{{ route('add-explicit') }}">
                        <span class="glyphicon glyphicon-share-alt"></span> Add Explicit
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('shows-my-explicit') }}">
                        <span class="glyphicon glyphicon-share-alt"></span> Your Explicit Data
                        </a>
                    </li>
                </ul>
            </li>
          
            <li class="parent ">
                <a href="#">
                   <span data-toggle="collapse" href="#sub-item-2"> <span class="glyphicon glyphicon-th" ></span> TACIT <span class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span> 
                </a>
                <ul class="children collapse" id="sub-item-2">
                    <li>
                        <a class="" href="{{ route('add-tacit') }}">
                            <span class="glyphicon glyphicon-share-alt"></span> Add Tacit
                        </a>
                    </li>
                    <li>
                        <a class="" href="{{ route('shows-tacit') }}">
                            <span class="glyphicon glyphicon-share-alt"></span> Tacit Data
                        </a>
                    </li>
                </ul>
            </li>

            <li role="presentation" class="divider"></li>
        </ul>

        <div class="attribution">KMS Beta 0.0</a></div>
    </div>
