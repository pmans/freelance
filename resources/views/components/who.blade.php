@if(Auth::guard('web')->check())
    <p class="text-success">
        You are logged In as a <strong> User </strong>
    </p>
    @else
    <p class="text-danger">
        You are Logged out as a <strong> User </strong>
    </p>
@endif

@if(Auth::guard('admin')->check())
    <p class="text-success">
        You are logged In as a <strong> Admin </strong>
    </p>
    @else
    <p class="text-danger">
        You are Logged out as a <strong> Admin </strong>
    </p>
@endif

@if(Auth::guard('pakar')->check())
    <p class="text-success">
        You are logged In as a <strong> Pakar </strong>
    </p>
    @else
    <p class="text-danger">
        You are Logged out as a <strong> Pakar </strong>
    </p>
@endif