@extends('layouts.master')
@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">

                    <div class="panel-heading">
                        <h4 class="panel-title"> <strong>{{ $explicit->title }}</strong></h4>
                        <h6><ul class="list-inline">
                            <li>
                                <span class="label label-info"><span class="glyphicon glyphicon-calendar"></span>{{ $explicit->created_at }}</span>
                                <span class="label label-info"><span class="glyphicon glyphicon-time"></span>{{ $explicit->created_at->diffForHumans() }}</span>
                                <span class="label label-info"><span class="glyphicon glyphicon-user"></span>Posting by {{ $explicit->user->name }}</span>
                            </li>
                        </ul></h6>
                     </div> 

                    <div class="panel-body">
                        <div class="col-md-12">
                           <?php echo $explicit->description ?><p>
                        <div class="alert alert-info">
                            <strong>URL :</strong> <i><a href="{{ $explicit->url }}" target="_blank">{{ $explicit->url }}</a></i>
                        </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>? ASK QUESTION</strong>
                    </div>

                    @foreach ($explicit->questions()->get() as $question)
                        <div class="col-md-12">
                            <h4><span class="label label-primary">{{ $question->user->name }} </span>
                            <div class="comment-wrap">
                                <div class="comment-block">
                                    <p class="comment-text"><?php echo $question->question ?></p>
                                    <div class="bottom-comment">
                                    <div class="comment-date">{{ $question->created_at->diffForHumans() }}</div>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endforeach

                    @foreach ($explicit->answers()->get() as $answer)
                        <div class="col-md-12">
                            <h4><span class="label label-danger">{{ $answer->pakar->name }} </span>
                            <div class="comment-wrap">
                                <div class="comment-block">
                                    <p class="comment-text"><?php echo $answer->answer ?></p>
                                    <div class="bottom-comment">
                                    <div class="comment-date">{{ $answer->created_at->diffForHumans() }}</div>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="panel-body">
                        <div class="col-md-12">
                            <form action="{{ route('question.store', $explicit) }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Question</label>
                                    <textarea name="question" id="ckeditor" required></textarea>

                            <div class="form-group">
                                <input name="explicit_id" class="form-control" type="hidden" value="{{ $explicit->id }}">
                            </div>

                            </div></div>
                            <button type="submit" class="btn btn-info btn-fill btn-wd"><span class="glyphicon glyphicon-send"></span> Send Question</button>      
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection