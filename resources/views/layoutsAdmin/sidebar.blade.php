
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

        <ul class="nav menu">
            <li class="list-group-item">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" ></span> {{ Auth::user()->name }} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <span class="glyphicon glyphicon-log-out"></span> Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}
                    </form>
                    </li>
                </ul>
            </li>
        </ul>

           
          
        <ul class="nav menu">
            <li><a href="{{ route('admin.home') }}"><span class="glyphicon glyphicon-dashboard"></span> DASBOARD </a></li>
            <li class="parent ">  
                <a href="#">
                    <span data-toggle="collapse" href="#sub-item-1"><span class="glyphicon glyphicon-th"></span> MANAGE USERS <span class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span></span>
                </a>
                <ul class="children collapse" id="sub-item-1">
                    <li>
                        <a href="{{ route('admin.admin') }}">
                        <span class="glyphicon glyphicon-share-alt"></span> Admin
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.pakar') }}">
                        <span class="glyphicon glyphicon-share-alt"></span> Pakar
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.karyawan') }}">
                        <span class="glyphicon glyphicon-share-alt"></span> Karyawan
                        </a>
                    </li>
                </ul>
            </li>
          
            <li><a href="{{ route('admin.explicit') }}"><span class="glyphicon glyphicon-book"></span> MANAGE EXPLICITS </a></li>
            <li><a href="{{ route('admin.tacit') }}"><span class="glyphicon glyphicon-list"></span> MANAGE TACITS </a></li>
            

            <li role="presentation" class="divider"></li>
        </ul>

        <div class="attribution">KMS Beta 0.0</a></div>
    </div>
