<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home/{explicit}/detail-explicit', 'HomeController@detail')->where('id', '\d+')->name('detail-exp');
Route::post('/home/{id}/question', 'QuestionController@store')->name('question.store');
Route::post('/home/{id}/answer', 'AnswerController@store')->name('answer.store');


//pakar
Route::group(['prefix' => 'pakar' ], function(){
    Route::get('/login', 'AuthPakar\LoginController@showLoginForm')->name('pakar.login');
    Route::post('/login', 'AuthPakar\LoginController@login')->name('pakar.login.submit');
    Route::get('/', 'PakarController@index')->name('pakar.home');
    Route::get('/add-explicit', 'ExplicitController@showAdd')->name('add-explicit');
    Route::post('/add-explicit', 'ExplicitController@create')->name('post-explicit');
    Route::get('/my-explicit', 'ExplicitController@show')->name('shows-my-explicit');
    Route::get('/shows-explicit/{explicit}/detail-shows-explicit', 'ExplicitController@detail')->where('id', '\d+')->name('detail-explicit');
    Route::get('/{id}/delete-explicit', 'ExplicitController@delete')->name('delete-explicit');
    Route::get('/{id}/edit-explicit', 'ExplicitController@showEditExplicit')->name('show.edit.explicit.pakar');
    Route::patch('/{id}/edit-explicit', 'ExplicitController@editExplicit')->name('edit.explicit.pakar');


    Route::get('/add-tacit', 'TacitController@showAdd')->name('add-tacit');
    Route::post('/add-tacit', 'TacitController@create')->name('post-tacit');
    Route::get('/shows-tacit', 'TacitController@show')->name('shows-tacit');
    Route::get('/{id}/delete-tacit', 'TacitController@delete')->name('delete-tacit');
    Route::get('/{id}/edit-tacit', 'TacitController@showEditTacit')->name('show.edit.tacit.pakar');
    Route::patch('/{id}/edit-tacit', 'TacitController@editTacit')->name('edit.tacit.pakar');
    Route::patch('/{id}/edit-status-tacit', 'TacitController@editStatusTacit')->name('edit.status.tacit.pakar');




});
//endpakar

//admin
Route::group(['prefix' => 'admin' ], function(){
    Route::get('/login', 'AuthAdmin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AuthAdmin\LoginController@login')->name('admin.login.submit');

    Route::get('/', 'AdminController@index')->name('admin.home');
    Route::get('/admin', 'AdminController@manageAdmin')->name('admin.admin');
    Route::get('/pakar', 'AdminController@managePakar')->name('admin.pakar');

    Route::get('/karyawan', 'AdminController@manageKaryawan')->name('admin.karyawan');
    Route::get('/karyawan/add', 'AdminController@showAdd')->name('show.add');
    Route::post('/karyawan/add', 'AdminController@addKaryawan')->name('add.karyawan');
    Route::get('/{id}/delete-karyawan', 'AdminController@deleteKaryawan')->name('delete.karyawan');
    Route::get('/{id}/edit-karyawan', 'AdminController@showEditKaryawan')->name('show.edit.karyawan');
    Route::patch('/{id}/edit-karyawan', 'AdminController@editKaryawan')->name('edit.karyawan');

    Route::get('/pakar/add', 'AdminController@showAddPakar')->name('show.add.pakar');
    Route::post('/pakar/add', 'AdminController@addPakar')->name('add.pakar');
    Route::get('/{id}/delete-pakar', 'AdminController@deletePakar')->name('delete.pakar');
    Route::get('/{id}/edit-pakar', 'AdminController@showEditPakar')->name('show.edit.pakar');
    Route::patch('/{id}/edit-pakar', 'AdminController@editPakar')->name('edit.pakar');

    Route::get('/admin/add', 'AdminController@showAddAdmin')->name('show.add.admin');
    Route::post('/admin/add', 'AdminController@addAdmin')->name('add.admin');
    Route::get('/{id}/delete-admin', 'AdminController@deleteAdmin')->name('delete.admin');
    Route::patch('//edit', 'AdminController@updateAdmin')->name('update.admin');
    Route::get('/{id}/edit-admin', 'AdminController@showEditAdmin')->name('show.edit.admin');
    Route::patch('/{id}/edit-admin', 'AdminController@editAdmin')->name('edit.admin');

    Route::get('/explicit', 'AdminController@manageExplicit')->name('admin.explicit');
    Route::get('/explicit/add', 'AdminController@showAddExplicit')->name('show.add.explicit');
    Route::post('/explicit/add', 'AdminController@addExplicit')->name('add.explicit');
    Route::get('/{id}/delete-explicit', 'AdminController@deleteExplicit')->name('delete.explicit');
    Route::get('/{id}/edit-explicit', 'AdminController@showEditExplicit')->name('show.edit.explicit');
    Route::patch('/{id}/edit-explicit', 'AdminController@editExplicit')->name('edit.explicit');

    Route::get('/tacit', 'AdminController@manageTacit')->name('admin.tacit');
    Route::get('/{id}/delete-tacit', 'AdminController@deleteTacit')->name('delete.tacit');
    Route::get('/{id}/edit-tacit', 'AdminController@showEditTacit')->name('show.edit.tacit');
    Route::patch('/{id}/edit-tacit', 'AdminController@editTacit')->name('edit.tacit');


});
//endadmin



