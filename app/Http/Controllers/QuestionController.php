<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\Question;

class QuestionController extends Controller
{
    public function store(Request $request, Explicit $explicit)
    {
        Question::create([
            'explicit_id' => request('explicit_id'),
            'user_id'     => auth()->id(),
            'question'    => $request->question
            ]);

        return redirect()->back();
    }
}
