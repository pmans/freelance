<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\Tacit;
use App\User;
use App\Pakar;
use App\Admin;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin       = \App\Admin::count();
        $pakar       = \App\Pakar::count();
        $user       = \App\User::count();
        $explicit   = \App\Explicit::count();
        $tacit      = \App\Tacit::count();  
        $explicits  = \App\Explicit::all();
        return view('admin.home', compact('explicits','explicit','tacit','admin','pakar','user'));
    }

    // karyawan
    public function manageKaryawan()
    {
        $users = \App\User::all();
        return view('admin.karyawan', compact('users'));
    }

    public function showAdd()
    {
        return view('admin.add-karyawan');
    }

    public function addKaryawan()
    {

        $this->validate(request(), [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',

        ]);

        User::create([
            'name'      => request('name'),
            'email'     => request('email'),
            'password'  => bcrypt(request('password'))
            ]);

        return redirect('admin/karyawan')->withInfo('Karyawan Successfully Add!!!');
    }

    public function deleteKaryawan($id)
    {
        $users = User::find($id)->delete();

        return redirect('admin/karyawan')->withDanger('Karyawan Successfully Delete!!!');

    }

    public function showEditKaryawan($id)
    {
        $user = User::findOrFail($id);
        return view('admin.edit-karyawan', compact('user'));
    }

    public function editKaryawan(Request $request, $id)
    {
        $this->validate(request(), [
            'name'         => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
            ]);

        User::where('id',$id)
        ->update([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
        ]);
       
       
        return redirect(route('admin.karyawan'))->withInfo('Karyawan Successfully Edit!!!');
    }
    //emdkaryawan


    //pakar
    public function managePakar()
    {
        $pakars = \App\Pakar::all();
        return view('admin.pakar', compact('pakars'));
    }

    public function showAddPakar()
    {
        return view('admin.add-pakar');
    }

    public function addPakar()
    {
        $this->validate(request(), [
            'name'         => 'required|string|max:255',
            'position'     => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
        ]);

        Pakar::create([
            'name'        => request('name'),
            'position'    => request('position'),
            'email'       => request('email'),
            'password'    => bcrypt(request('password'))
            ]);

        return redirect('admin/pakar')->withInfo('Pakar Successfully Add!!!');
    }

    public function deletePakar($id)
    {
        $pakars = Pakar::find($id)->delete();
        return redirect('admin/pakar')->withDanger('Pakar Successfully Delete!!!');
    }

    public function showEditPakar($id)
    {
        $pakar = Pakar::findOrFail($id);
        return view('admin.edit-pakar', compact('pakar'));
    }

    public function editPakar(Request $request, $id)
    {
        $this->validate(request(), [
            'name'         => 'required|string|max:255',
            'position'     => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
            ]);

        Pakar::where('id',$id)
        ->update([
            'name'      => $request->name,
            'position'  => $request->position,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
        ]);
       
        return redirect(route('admin.pakar'))->withInfo('Pakar Successfully Edit!!!');
    }

    //endpakar

    //admin
    public function manageAdmin()
    {
        $admins = \App\Admin::all();
        return view('admin.admin', compact('admins'));
    }

    public function showAddAdmin()
    {
        return view('admin.add-admin');
    }

    public function addAdmin()
    {
        $this->validate(request(), [
            'name'         => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
        ]);

        Admin::create([
            'name'        => request('name'),
            'email'       => request('email'),
            'password'    => bcrypt(request('password'))
            ]);

        return redirect('admin/admin')->withInfo('Admin Successfully Add!!!');
    }

    public function deleteAdmin($id)
    {
        $admins = Admin::find($id)->delete();
        return redirect('admin/admin')->withDanger('Admin Successfully Delete!!!');
    }

    public function showEditAdmin($id)
    {
        $admin = Admin::findOrFail($id);
        return view('admin.edit-admin', compact('admin'));

    }

    public function editAdmin(Request $request, $id)
    {
        $this->validate(request(), [
            'name'         => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
        ]);

        Admin::where('id',$id)
        ->update([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
        ]);
       
        return redirect(route('admin.admin'))->withInfo('Admin Successfully Add!!!');
    }

    //endadmin


   //explicit
    public function manageExplicit()
    {
        $explicits = \App\Explicit::all();
        return view('admin.explicit', compact('explicits'));
    }

    public function showAddExplicit()
    {
        return view('admin.add-explicit');
    }

    public function addExplicit()
    {
        $this->validate(request(), [
            'title'         => 'required',
            'description'   => 'required|min:25',
            'url'           => 'required',

        ]);

        Explicit::create([
            'title'         => request('title'),
            'description'   => request('description'),
            'url'           => request('url'),
            'user_id'       => request('user_id')
            ]);

        return redirect('admin/explicit')->withInfo('Explicit Successfully Add!!!');
    }

    public function deleteExplicit($id)
    {
        $explicit = Explicit::find($id)->delete();
        return redirect('admin/explicit')->withDanger('Explicit Successfully Delete!!!');
    }

    public function showEditExplicit($id)
    {
        $explicit = Explicit::findOrFail($id);
        return view('admin.edit-explicit', compact('explicit'));
    }

    public function editExplicit(Request $request, $id)
    {
        Explicit::where('id',$id)
        ->update([
            'title'         => $request->title,
            'description'   => $request->description,
            'url'           => $request->url
        ]);
       
        return redirect(route('admin.explicit'));
    }
    //endexplicit

    //tacit
    public function manageTacit()
    {
        $tacits = \App\Tacit::all();
        return view('admin.tacit', compact('tacits'));
    }

    public function deleteTacit($id)
    {
        $tacits = Tacit::find($id)->delete();
        return redirect('admin/tacit')->withDanger('Tacit Successfully Delete!!!');
    }

    public function showEditTacit($id)
    {
        $tacit = Tacit::findOrFail($id);
        return view('admin.edit-tacit', compact('tacit'));
    }

    public function editTacit(Request $request, $id)
    {
        Tacit::where('id',$id)
        ->update([
            'complain'      => $request->complain,
            'action'        => $request->action,
            'information'   => $request->information,
            'status'        => $request->status

        ]);
       
        return redirect(route('admin.tacit'))->withInfo('Tacit Successfully Delete!!!');
    }


    //endtacit

}
