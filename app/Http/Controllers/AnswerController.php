<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\Answer;

class AnswerController extends Controller
{
    public function store(Request $request, Explicit $explicit)
    {
        Answer::create([
            'explicit_id' => request('explicit_id'),
            'pakar_id'     => auth()->id(),
            'answer'    => $request->answer
            ]);

        return redirect()->back();
    }
}
