<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\Tacit;

class PakarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:pakar');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \App\User::count();
        $explicit = \App\Explicit::count();
        $tacit = \App\Tacit::count();

        $explicits = \App\Explicit::all();

        return view('pakar.home', compact('explicits','explicit','tacit'));
    }
}
