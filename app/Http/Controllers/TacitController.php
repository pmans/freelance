<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tacit;

class TacitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:pakar');
    }

    public function showAdd()
    {
        return view('pakar/add-tacit');
    }

    public function create()
    {
        $this->validate(request(), [
            'complain'      => 'required|min:10',
            'action'        => 'required|min:10',
            'information'   => 'required',
        ]);

        Tacit::create([
            'complain'      => request('complain'),
            'action'        => request('action'),
            'information'   => request('information'),
            'status'        => request('status'),
            'user_id'       => request('user_id')
            ]);
        return redirect('pakar/add-tacit')->withInfo('Tacit Successfully Add!!!');
    }

    public function show()
    {
        $tacits = \App\Tacit::all();
        return view('pakar.shows-tacit', compact('tacits'));        
    }

    public function delete($id)
    {
        $tacits = Tacit::find($id)->delete();
        return redirect('pakar/shows-tacit')->withDanger('Tacit Successfully Delete!!!');
    }

    public function showEditTacit($id)
    {
        $tacit = Tacit::findOrFail($id);
        return view('pakar.edit-tacit', compact('tacit'));
    }

    public function editTacit(Request $request, $id)
    {
        Tacit::where('id',$id)
        ->update([
            'complain'      => $request->complain,
            'action'        => $request->action,
            'information'   => $request->information
        ]);
       
        return redirect(route('shows-tacit'))->withInfo('Tacit Successfully Edit!!!');
    }


}
