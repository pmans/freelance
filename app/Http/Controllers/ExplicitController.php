<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;

class ExplicitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:pakar');
        $this->explicits = Explicit::all();
    }
    
    public function showAdd()
    {
        return view('pakar/add-explicit');
    }

    public function create()
    {
        $this->validate(request(), [
            'title'         => 'required',
            'description'   => 'required|min:25',
            'url'           => 'required',
        ]);

        Explicit::create([
            'title'         => request('title'),
            'description'   => request('description'),
            'url'           => request('url'),
            'user_id'       => request('user_id')
            ]);

        return redirect('pakar/add-explicit')->withInfo('Explicit Successfully Add!!!');
    }

    public function show()
    {
        $explicits = $this->explicits;
        $explicits = \App\Explicit::all();
        return view('pakar.shows-my-explicit', compact('explicits'));        
    }

    public function detail(Explicit $explicit)
    {   
        return view('pakar.detail-show-explicit', compact('explicit'));
    }

    public function delete($id)
    {
        $explicits = Explicit::find($id)->delete();
        return redirect('pakar/my-explicit')->withDanger('Explicit Successfully Delete!!!');
    }

    public function showEditExplicit($id)
    {
        $explicit = Explicit::findOrFail($id);
        return view('pakar.edit-explicit', compact('explicit'));
    }

    public function editExplicit(Request $request, $id)
    {
        Explicit::where('id',$id)
        ->update([
            'title'         => $request->title,
            'description'   => $request->description,
            'url'           => $request->url
        ]);
       
        return redirect(route('shows-my-explicit'))->withInfo('Explicit Successfully Edit!!!');
    }
}
