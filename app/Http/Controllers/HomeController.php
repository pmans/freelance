<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Explicit;
use App\Tacit;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
        $this->explicits = Explicit::all();
        $this->tacits = Tacit::all();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $user        = \App\User::count();
        $explicit   = \App\Explicit::count();
        $tacit      = \App\Tacit::count();
        $explicits  = \App\Explicit::all();
        $tacits     = \App\Tacit::all();

        return view('home', compact('explicits','explicit','tacits','tacit'));
    }

    public function detail(Explicit $explicit)
    {   
        return view('detail-explicit', compact('explicit'));
    }
}
