<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tacit extends Model
{
    protected $table = 'tacits';
    protected $fillable = ['complain','action','information','status','user_id'];

    public function pakar()
    {
        return $this->belongsTo('App\Pakar', 'user_id');
    }
}
