<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'pakar_id',
        'explicit_id',
        'answer',
    ];

    public function explicit()
    {
        return $this->belongsTo(Explicit::class);
    }

    public function pakar()
    {
        return $this->belongsTo(Pakar::class);
    }

}
