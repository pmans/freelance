<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'user_id',
        'explicit_id',
        'question',
    ];

    public function explicit()
    {
        return $this->belongsTo(Explicit::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
