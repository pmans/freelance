<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Explicit extends Model
{
    protected $table = 'explicits';
    protected $fillable = ['title','description','url','user_id'];

    public function user()
     {
         return $this->belongsTo('App\Pakar', 'user_id');
     }

    public function questions() 
    {
        return $this->hasMany(Question::class);
    }

    public function answers() 
    {
        return $this->hasMany(Answer::class);
    }
}
